import React from "react";
import { ColumnsType } from "antd/es/table";
import * as Styled from "../ShoppingCart.styles";
import { Image } from "antd";
import { MinusOutlined, PlusOutlined } from "@ant-design/icons";

interface IProps {
  addBook: (id: number) => void;
  deleteBook: (id: number) => void;
  renderPrice: (price: number) => string;
}

export const Columns = ({
  addBook,
  deleteBook,
  renderPrice,
}: IProps): ColumnsType<PurchasedBooks> => [
  {
    render: (_, { book, quantity }) => (
      <Styled.QuantityBookContainer>
        <Image src={book.cover_url} width={100} />
        <Styled.ButtonContainer>
          <Styled.Button
            onClick={() => deleteBook(book.id)}
            icon={<MinusOutlined />}
          />
          <Styled.Input value={quantity} />
          <Styled.Button
            onClick={() => addBook(book.id)}
            icon={<PlusOutlined />}
          />
        </Styled.ButtonContainer>
        <Styled.Price>
          Cena: {renderPrice(book.price * quantity)} zł
        </Styled.Price>
      </Styled.QuantityBookContainer>
    ),
  },
];
