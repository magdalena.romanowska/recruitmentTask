import React from "react";
import "../../../jest.setup";
import { render, screen } from "@testing-library/react";
import ShoppingCart from "../ShoppingCart";
import { BookContext } from "../../../context/bookContext";

import {
  mockBooks,
  mockMetaData,
  mockPurchasedBooks,
} from "../../../assets/__mocks__/mockBooks";

const createValue = (hasBooks: boolean) => ({
  books: [mockBooks],
  pageData: mockMetaData,
  purchasedBooks: hasBooks ? mockPurchasedBooks : [],
  addBookToBasket: jest.fn(),
  addBook: jest.fn(),
  deleteBook: jest.fn(),
  addPricesOrder: jest.fn(),
  cleanBasket: jest.fn(),
});

describe("Shopping cart", () => {
  test("Should display shopping cart with data", async () => {
    render(
      <BookContext.Provider value={createValue(true)}>
        <ShoppingCart />
      </BookContext.Provider>
    );

    expect(screen.getByText(/Podsumowanie zamówienia/)).toBeInTheDocument();
    expect(screen.getByText(/Zamówienie/)).toBeInTheDocument();

    expect(screen.getByRole("button", { name: /Dalej/i })).not.toBeDisabled();
  });

  test("Display shopping cart without data", () => {
    render(
      <BookContext.Provider value={createValue(false)}>
        <ShoppingCart />
      </BookContext.Provider>
    );

    const button = screen.getByRole("button", { name: /Dalej/i });

    expect(button).toBeDisabled();
  });
});
