import React from "react";
import styled from "styled-components";

import {
  Button as AntdButton,
  Input as AntdInput,
  Empty as AntdEmpty,
  Table as AntdTable,
} from "antd";

export const Container = styled.div`
  display: grid;
`;

export const TitleContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export const Title = styled.div`
  margin: 15px;
  font-family: Courgette;
  font-size: 35px;
  color: rgb(103, 103, 103);
`;

export const LineTop = styled.div`
  border-top: 1px solid rgba(84, 84, 84, 0.22);
`;

export const LineBottom = styled.div`
  border-bottom: 1px solid rgba(84, 84, 84, 0.22);
  height: 10px;
  margin-bottom: 5px;
`;

export const BooksList = styled.div`
  justify-self: center;
  width: calc(100% - 600px);
  height: 1000px;
  background-color: #ffffff;
`;

export const QuantityContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 30px 30px 30px 30px;
`;

export const QuantityBookContainer = styled.div`
  display: flex;
  margin: 50px 30px 50px 30px;
`;

export const BooksContainer = styled.div`
  display: grid;
  border: rgba(119, 119, 119, 0.41) 1px solid;
`;

export const Input = styled(AntdInput)`
  height: 45px;
  width: 60px;
  margin: 0px 25px;
  font-weight: bold;
  text-align: center;
`;

export const Button = styled(AntdButton)`
  height: 30px;
  border: none;
`;

export const ButtonContainer = styled.div`
  margin-left: 30px;
`;

export const Price = styled.div`
  margin-left: 20px;
  margin-top: 10px;
  font-weight: bold;
`;

export const Summary = styled.div`
  font-size: 25px;
  height: 55px;
  justify-content: center;
`;

export const OrderSummaryText = styled.div`
  margin-top: 15px;
  font-family: Courgette;
  color: rgb(103, 103, 103);
`;

export const Total = styled.strong``;

export const OrderContainer = styled.div`
  display: grid;
  justify-content: center;
  border: rgba(119, 119, 119, 0.41) 1px solid;
  width: 500px;
  height: 250px;
  margin-left: 150px;
`;

export const PriceContainer = styled.div`
  display: flex;
  justify-content: right;
  font-size: 15px;
`;

export const OrderPrice = styled.div`
  margin-left: 30px;
`;

export const AddOrderButton = styled(AntdButton)`
  align-self: end;
  justify-self: center;
  margin-bottom: 30px;
  width: 200px;
  height: 50px;
  border-radius: 10px;
  background-color: #6699cc;
  border: none;
  :hover {
    background-color: #276c9b;
  }
`;

export const Empty = styled(AntdEmpty)`
  margin: 30px;
  width: 300px;
`;

export const Text = styled.div`
  font-size: 20px;
  font-family: Courgette;
  color: rgb(255, 255, 255);
`;

export const Table = styled(({ ...props }) => <AntdTable {...props} />)`
  .ant-table-tbody {
    margin: 0px;
  }
  .ant-table table {
    border-collapse: collapse;
  }

  .ant-table-cell {
    background-color: white;
  }

  .ant-pagination-item-link {
    border-radius: 5px;
    border-color: #6699cc;

    :hover {
      background-color: #6699cc;
      color: #ffffff;
    }
    color: #276c9b;
  }

  .ant-pagination-item {
    border-radius: 5px;
    color: #ffffff;
    border-color: #6699cc;

    :hover {
      background-color: #6699cc;
      color: #ffffff;
    }
    color: #276c9b;
  }

  .ant-pagination-item a {
    color: #6699cc;
  }

  .ant-pagination-item:hover a {
    color: #ffffff;
  }

  .ant-pagination-item-active a {
    background-color: #6699cc;
    color: #ffffff;
    font-weight: bold;
  }

  .ant-pagination-item-active:hover a {
    background-color: #6699cc;
    color: #ffffff;
  }

  .ant-table-placeholder {
    width: 150px;
  }
`;
