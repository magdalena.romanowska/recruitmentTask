import React from "react";
import { Columns } from "./columns/columns";
import * as Styled from "./ShoppingCart.styles";

import { useBook } from "../../context/bookContext";
import { history } from "../../helpers/History";

const ShoppingCart = () => {
  const { purchasedBooks, addBook, deleteBook, addPricesOrder } = useBook();

  const nextPath = () => {
    history.push("/order-form");
  };

  const renderPrice = (price: number) =>
    parseFloat((price / 100).toString()).toFixed(2);

  return (
    <>
      <Styled.Container>
        <Styled.BooksList>
          <Styled.TitleContainer>
            <Styled.Title>Zamówienie</Styled.Title>
          </Styled.TitleContainer>
          <Styled.LineTop />
          <Styled.QuantityContainer>
            <Styled.BooksContainer>
              <Styled.Table
                dataSource={purchasedBooks.length ? purchasedBooks : []}
                columns={Columns({ addBook, deleteBook, renderPrice })}
                pagination={{
                  pageSize: 3,
                  position: ["bottomCenter"],
                }}
                locale={<Styled.Empty description={false} />}
                showHeader={false}
              />
            </Styled.BooksContainer>

            <Styled.OrderContainer>
              <Styled.Summary>
                <Styled.OrderSummaryText>
                  Podsumowanie zamówienia
                </Styled.OrderSummaryText>
                <Styled.LineBottom />
                <Styled.PriceContainer>
                  <Styled.Total>RAZEM:</Styled.Total>
                  <Styled.OrderPrice>{addPricesOrder()} zł</Styled.OrderPrice>
                </Styled.PriceContainer>
              </Styled.Summary>
              <Styled.AddOrderButton
                onClick={nextPath}
                disabled={!purchasedBooks.length}
              >
                <Styled.Text>Dalej</Styled.Text>
              </Styled.AddOrderButton>
            </Styled.OrderContainer>
          </Styled.QuantityContainer>
        </Styled.BooksList>
      </Styled.Container>
    </>
  );
};

export default ShoppingCart;
