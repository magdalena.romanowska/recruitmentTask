import React from "react";
import {
  Route,
  Switch,
  Router as ReactRouter,
  Redirect,
} from "react-router-dom";

import { Dashboard } from "../Dashboard/Dashboard";
import { history } from "../../helpers/History";
import { AppProvider } from "../../providers";

export const Router = () => (
  <ReactRouter history={history}>
    <AppProvider>
      <Switch>
        <Route exact path="/" component={() => <Redirect to="/home" />} />
        <Route path="/" component={Dashboard} />
      </Switch>
    </AppProvider>
  </ReactRouter>
);
