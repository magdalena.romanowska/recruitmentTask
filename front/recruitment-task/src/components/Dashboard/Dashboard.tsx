import React from "react";
import { Switch, Route } from "react-router-dom";

import ShoppingCart from "../ShoppingCart/ShoppingCart";
import OrderForm from "../OrderForm/OrderForm";
import Home from "../Home/Home";
import Result from "../Result/Result";
import Navbar from "../Navbar/Navbar";

export const Dashboard = () => (
  <>
    <Navbar />
    <Switch>
      <Route path="/home" component={Home} />
      <Route path="/shopping-cart" component={ShoppingCart} />
      <Route path="/order-form" component={OrderForm} />
      <Route path="/success" component={Result} />
    </Switch>
  </>
);
