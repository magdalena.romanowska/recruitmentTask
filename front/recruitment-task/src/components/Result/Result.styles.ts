import styled from "styled-components";

export const Title = styled.div`
  font-size: 30px;
  font-family: Courgette;
  color: #565656;
`;

export const SubTitle = styled.div`
  font-size: 20px;
  font-family: Courgette;
  color: #565656;
`;
