import React from "react";
import * as Styled from "./Result.styles";

import { Result as AntdResult } from "antd";
import { history } from "../../helpers/History";

const returnHome = () => {
  setTimeout(() => history.push("/home"), 5000);
};

const Result = () => (
  <>
    <AntdResult
      status="success"
      title={<Styled.Title>Zamówienie zostało złożone!</Styled.Title>}
      subTitle={
        <Styled.SubTitle>
          Za chwilę zostaniesz przekierowany na stronę główną.
        </Styled.SubTitle>
      }
    />
    {returnHome()}
  </>
);

export default Result;
