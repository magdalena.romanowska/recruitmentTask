import styled from "styled-components";
import { NavLink as ReactNavLink } from "react-router-dom";
import { ShoppingCartOutlined } from "@ant-design/icons";

export const Header = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
`;

export const NavLink = styled(ReactNavLink)`
  margin-left: 50px;
  align-self: center;
`;

export const Title = styled.div`
  font-family: Courgette;
  font-size: 45px;
  color: #6699cc;
`;

export const IconContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: right;
  margin-right: 50px;
`;

export const ShoppingCartIcon = styled(ShoppingCartOutlined)`
  font-size: 35px;
  color: #595959;
`;
