import React, { useEffect, useState } from "react";
import { useBook } from "../../context/bookContext";

import * as Styled from "./Navbar.styles";
import { Badge } from "antd";

const Navbar = () => {
  const { purchasedBooks } = useBook();
  const [booksList, setBooksList] = useState(0);

  useEffect(() => {
    const booksQuantity = purchasedBooks.length
      ? purchasedBooks
          .map(({ quantity }) => quantity)
          .reduce((previousValue, currentValue) => previousValue + currentValue)
      : 0;

    setBooksList(booksQuantity);
  }, [purchasedBooks]);

  return (
    <div style={{ display: "flex" }}>
      <Styled.Header>
        <Styled.NavLink to="/home">
          <Styled.Title>Strona główna</Styled.Title>
        </Styled.NavLink>
      </Styled.Header>
      <Styled.IconContainer>
        <Styled.NavLink to="/shopping-cart">
          <Badge count={booksList} color={"#276c9b"}>
            <Styled.ShoppingCartIcon />
          </Badge>
        </Styled.NavLink>
      </Styled.IconContainer>
    </div>
  );
};

export default Navbar;
