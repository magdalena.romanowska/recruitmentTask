import styled from "styled-components";

import { Form as AntdForm, Button as AntdButton } from "antd";

export const Container = styled.div`
  display: grid;
`;

export const TitleContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export const Title = styled.div`
  font-size: 35px;
  color: rgb(103, 103, 103);
  margin: 15px;
  font-family: Courgette;
`;

export const LineTop = styled.div`
  border-top: 1px solid rgba(84, 84, 84, 0.22);
`;

export const FormArea = styled.div`
  justify-self: center;
  width: calc(100% - 467px);
  height: 1000px;
  background-color: #ffffff;
`;

export const OrderForm = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 30px;
`;

export const FormFields = styled.div`
  width: 550px;
  height: 400px;
  border: rgba(108, 108, 108, 0.27) 1px solid;
  display: grid;
  justify-content: center;
`;

export const Form = styled(AntdForm)`
  margin: 20px 20px 20px 20px;
  width: 500px;
`;

export const Button = styled(AntdButton)`
  align-self: end;
  justify-self: center;
  margin: 0px 20px 30px 20px;
  height: 50px;
  width: 350px;
  border-radius: 10px;
  background-color: #6699cc;
  border: none;
  :hover {
    background-color: #276c9b;
  }
`;

export const Text = styled.div`
  font-size: 20px;
  font-family: Courgette;
  color: rgb(255, 255, 255);
`;
