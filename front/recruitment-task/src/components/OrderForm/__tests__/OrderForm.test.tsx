require("react");
import React from "react";
import "../../../jest.setup";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import OrderForm from "../OrderForm";
import { BookContext } from "../../../context/bookContext";

import {
  mockBooks,
  mockMetaData,
  mockPurchasedBooks,
} from "../../../assets/__mocks__/mockBooks";

const clean = jest.fn();

const createValue = () => ({
  books: [mockBooks],
  pageData: mockMetaData,
  purchasedBooks: mockPurchasedBooks,
  addBookToBasket: jest.fn(),
  addBook: jest.fn(),
  deleteBook: jest.fn(),
  addPricesOrder: jest.fn(),
  cleanBasket: clean,
});

const hasInputValue = (
  e: Document | Element | Window | Node,
  inputValue: string
) => screen.getByDisplayValue(inputValue) === e;

const renderOrderForm = () =>
  render(
    <BookContext.Provider value={createValue()}>
      <OrderForm />
    </BookContext.Provider>
  );

describe("Order Form", () => {
  test("Should display order form", async () => {
    renderOrderForm();

    expect(screen.getByText(/Formularz zamówienia/i)).toBeInTheDocument();

    expect(
      screen.getByRole("button", {
        name: /Zamawiam i płacę/i,
      })
    ).toBeInTheDocument();

    expect(screen.getByLabelText("Imię")).toBeInTheDocument();
    expect(screen.getByLabelText("Nazwisko")).toBeInTheDocument();
    expect(screen.getByLabelText("Miejscowość")).toBeInTheDocument();
    expect(screen.getByLabelText("Kod pocztowy")).toBeInTheDocument();
  });

  test("Should complete the form and place an order", async () => {
    renderOrderForm();

    const orderButton = screen.getByRole("button", {
      name: /Zamawiam i płacę/i,
    });
    expect(orderButton).toBeInTheDocument();

    const nameInput = screen.getByLabelText("Imię");
    const surnameInput = screen.getByLabelText("Nazwisko");
    const cityInput = screen.getByLabelText("Miejscowość");
    const zipcodeInput = screen.getByLabelText("Kod pocztowy");

    fireEvent.change(nameInput, { target: { value: "Anna" } });
    expect(hasInputValue(nameInput, "Anna")).toBe(true);

    fireEvent.change(surnameInput, { target: { value: "Kowalska" } });
    expect(hasInputValue(surnameInput, "Kowalska")).toBe(true);

    fireEvent.change(cityInput, { target: { value: "Warszawa" } });
    expect(hasInputValue(cityInput, "Warszawa")).toBe(true);

    fireEvent.change(zipcodeInput, { target: { value: "00-155" } });
    expect(hasInputValue(zipcodeInput, "00-155")).toBe(true);

    fireEvent.click(orderButton);
    await waitFor(() => {
      expect(clean).toBeCalledTimes(1);
    });
  });
});
