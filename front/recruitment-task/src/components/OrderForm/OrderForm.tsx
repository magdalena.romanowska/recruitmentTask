import React from "react";
import { useBook } from "../../context/bookContext";

import { submitOrder } from "../../services/book";
import { history } from "../../helpers/History";

import * as Styled from "./OrderForm.styles";
import { Form, Input } from "antd";

const OrderForm = () => {
  const { purchasedBooks, cleanBasket } = useBook();
  const [form] = Form.useForm();

  const savePayload = () => {
    form.validateFields().then(({ first_name, last_name, city, zip_code }) => {
      const order = purchasedBooks.map(({ book, quantity }) => ({
        id: book.id,
        quantity,
      }));

      const payload: PayloadOrder = {
        order,
        first_name,
        last_name,
        city,
        zip_code,
      };

      submitOrder(payload).then(() => {
        form.resetFields();
        cleanBasket();

        history.push("/success");
      });
    });
  };

  return (
    <Styled.Container>
      <Styled.FormArea>
        <Styled.TitleContainer>
          <Styled.Title>Formularz zamówienia</Styled.Title>
        </Styled.TitleContainer>
        <Styled.LineTop />
        <Styled.OrderForm>
          <Styled.FormFields>
            <Styled.Form form={form}>
              <Form.Item
                label={"Imię"}
                name={"first_name"}
                rules={[
                  {
                    required: true,
                    message:
                      "Minimalna liczba znaków: 4. Dozwolone znaki: litery alfabetu. ",
                    pattern: new RegExp(/^[A-Za-z]{4,}$/),
                  },
                ]}
              >
                <Input placeholder={"Podaj imię"} />
              </Form.Item>
              <Form.Item
                label={"Nazwisko"}
                name={"last_name"}
                rules={[
                  {
                    required: true,
                    message:
                      "Minimalna liczba znaków: 5. Dozwolone znaki: litery alfabetu.",
                    pattern: new RegExp(/^[a-zA-Z]{5,}$/),
                  },
                ]}
              >
                <Input placeholder={"Podaj nazwisko"} />
              </Form.Item>
              <Form.Item
                label={"Miejscowość"}
                name={"city"}
                rules={[
                  {
                    required: true,
                    message: "Podaj miejscowość. Dozwolone znaki: litery alfabetu",
                    pattern: new RegExp(/^[a-zA-Z]{2,}$/),
                  },
                ]}
              >
                <Input placeholder={"Podaj miejscowość"} />
              </Form.Item>
              <Form.Item
                label={"Kod pocztowy"}
                name={"zip_code"}
                rules={[
                  {
                    required: true,
                    message: "Podany kod jest nie prawidłowy.",
                    pattern: new RegExp(/^([0-9]{2}-[0-9]{3})$/),
                  },
                ]}
              >
                <Input placeholder={"Np. 82-300"} />
              </Form.Item>
            </Styled.Form>

            <Styled.Button onClick={savePayload}>
              <Styled.Text>Zamawiam i płacę</Styled.Text>
            </Styled.Button>
          </Styled.FormFields>
        </Styled.OrderForm>
      </Styled.FormArea>
    </Styled.Container>
  );
};

export default OrderForm;
