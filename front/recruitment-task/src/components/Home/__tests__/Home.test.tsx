import "../../../jest.setup";
import React from "react";
import Home from "../Home";

import { render, screen } from "@testing-library/react";
import { BookContext } from "../../../context/bookContext";
import { mockBooks, mockMetaData } from "../../../assets/__mocks__/mockBooks";

const titles = ["Matematyka 1", "Podręcznik", "Zakres podstawowy"];

const value = {
  books: [mockBooks],
  pageData: mockMetaData,
  purchasedBooks: [],
  addBookToBasket: jest.fn(),
  addBook: jest.fn(),
  deleteBook: jest.fn(),
  addPricesOrder: jest.fn(),
  cleanBasket: jest.fn(),
};

describe("Home", () => {
  test("Display home", () => {
    render(
      <BookContext.Provider value={value}>
        <Home />
      </BookContext.Provider>
    );

    expect(
      screen.getByRole("button", { name: /Dodaj do koszyka/i })
    ).toBeInTheDocument();

    expect(screen.getByText(/Liczba stron: 280/i)).toBeInTheDocument();

    titles.forEach((title) => {
      expect(screen.getByText(`${title}`)).toBeInTheDocument();
    });

    expect(
      screen.getByText(/M. Karpiński, M. Dobrowolska, M. Braun, J. Lech/i)
    ).toBeInTheDocument();
  });
});
