import React, { useState } from "react";

import AddBookDrawer from "./AddBookDrawer/AddBookDrawer";
import { Columns } from "./columns/columns";

import { useBook } from "../../context/bookContext";

import * as Styled from "./Home.styles";

const Home = () => {
  const { books, addBookToBasket } = useBook();

  const [visibleModal, setVisibleModal] = useState(false);
  const [addedBook, setAddedBook] = useState<Book>();

  const addBook = (id: number) => {
    const previewBook = books.find((book) => book.id === id);

    if (previewBook) {
      setAddedBook(previewBook);
      setVisibleModal(true);
    }

    addBookToBasket(id);
  };

  const onCloseModal = () => {
    setVisibleModal(false);
    setAddedBook(undefined);
  };

  const renderTitle = (titleBook: string) => {
    const titleArray = titleBook.split(".");

    return titleArray.map((title) => <Styled.Title>{title}</Styled.Title>);
  };

  return (
    <Styled.Container>
      <Styled.Table
        dataSource={books.length ? books : []}
        columns={Columns({ addBook, renderTitle })}
        pagination={{
          pageSize: 4,
          position: ["bottomCenter"],
        }}
        locale={{ emptyText: "Brak danych." }}
      />

      <AddBookDrawer
        visible={visibleModal}
        closeModal={onCloseModal}
        book={addedBook}
      />
    </Styled.Container>
  );
};

export default Home;
