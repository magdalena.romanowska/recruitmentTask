import React from "react";
import styled from "styled-components";

import {
  Button,
  Card as AntdCard,
  Image as AntdImage,
  Table as AntdTable,
} from "antd";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 50px;
`;

export const Table = styled(({ ...props }) => <AntdTable {...props} />)`
  .ant-table-tbody {
    display: flex;
  }

  .ant-table table {
    border-collapse: collapse;
  }

  .ant-table-cell {
    background-color: white;
    margin: 0px; !important;
  }

  .ant-pagination-item-link {
    border-radius: 5px;
    border-color: #6699cc;

    :hover {
      background-color: #6699cc;
      color: #ffffff;
    }
    color: #276c9b;
  }

  .ant-pagination-item {
    border-radius: 5px;
    color: #ffffff;
    border-color: #6699cc;

    :hover {
      background-color: #6699cc;
      color: #ffffff;
    }
    color: #276c9b;
  }

  .ant-pagination-item a {
    color: #6699cc;
  }

  .ant-pagination-item:hover a {
    color: #ffffff;
  }

  .ant-pagination-item-active a {
    background-color: #6699cc;
    color: #ffffff;
    font-weight: bold;
  }

  .ant-pagination-item-active:hover a {
    background-color: #6699cc;
    color: #ffffff;
  }
`;

export const ContainerCard = styled.div`
  display: grid;
`;

export const Card = styled(AntdCard)`
  width: 400px;
  height: 400px;
  margin: 10px;

  .ant-image {
    display: flex;
    justify-content: center;
  }
  .ant-card-meta-title {
    display: grid;
  }
  .ant-card-meta-description {
    display: grid;
  }
`;

export const Title = styled.div`
  font-size: 15px;
  font-weight: bold;
  justify-self: center;
  color: rgb(65, 65, 65);
`;

export const Description = styled.div`
  justify-self: center;
  color: #282c34;
`;

export const AddBookButton = styled(Button)`
  justify-self: center;
  width: 230px;
  height: 50px;
  border-radius: 10px;
  background-color: #6699cc;
  border: none;
  :hover {
    background-color: #276c9b;
  }
`;

export const Text = styled.div`
  font-size: 20px;
  font-family: Courgette;
  color: #ffffff;
`;

export const Image = styled(AntdImage)`
  width: 150px;
  margin: 15px 10px 10px 10px;
`;
