import React from "react";
import { history } from "../../../helpers/History";

import * as Styled from "./AddBookDrawer.styles";
import { Image } from "antd";

interface IProps {
  visible: boolean;
  closeModal: () => void;
  book?: Book;
}

const AddBookDrawer = ({ visible, closeModal, book }: IProps) => {
  const nextPath = () => {
    closeModal();
    history.push("/shopping-cart");
  };

  const renderDetails = () => {
    if (book) {
      const { cover_url, price, title, author, currency } = book;

      return (
        <>
          <Styled.Container>
            <Image width={120} src={cover_url} />
            <Styled.Description>
              <Styled.Price>
                {parseFloat((price / 100).toString()).toFixed(2)}{" "}
                {currency ? "zł" : ""}
              </Styled.Price>
              <Styled.Title>{title}</Styled.Title>
              <Styled.Author>{author}</Styled.Author>
            </Styled.Description>
          </Styled.Container>
          <Styled.ButtonContainer>
            <Styled.Button onClick={nextPath}>
              <Styled.Text>Przejdź do koszyka</Styled.Text>
            </Styled.Button>
          </Styled.ButtonContainer>
        </>
      );
    }
  };

  return (
    <Styled.Drawer
      title={"Dodano do koszyka"}
      visible={visible}
      onClose={closeModal}
    >
      {renderDetails()}
    </Styled.Drawer>
  );
};

export default AddBookDrawer;
