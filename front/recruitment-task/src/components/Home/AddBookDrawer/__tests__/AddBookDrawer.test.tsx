import React from "react";
import AddBookDrawer from "../AddBookDrawer";

import { render, screen } from "@testing-library/react";

import { mockBooks } from "../../../../assets/__mocks__/mockBooks";

describe("Add Book Drawer", () => {
  test("Should display add book drawer", () => {
    render(
      <AddBookDrawer visible={true} closeModal={jest.fn()} book={mockBooks} />
    );

    expect(screen.getByText(/Matematyka 1. Podręcznik. Zakres podstawowy/i));
    expect(
      screen.getByText(/M. Karpiński, M. Dobrowolska, M. Braun, J. Lech/i)
    );

    expect(
      screen.getByRole("button", { name: /Przejdź do koszyka/i })
    ).toBeInTheDocument();
  });
});
