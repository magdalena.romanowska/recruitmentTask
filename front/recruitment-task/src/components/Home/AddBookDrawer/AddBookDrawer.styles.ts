import styled from "styled-components";

import { Drawer as AntdDrawer, Button as AntdButton } from "antd";

export const Drawer = styled(AntdDrawer)`
  .ant-drawer-content-wrapper {
    width: 450px !important;
  }
`;

export const Container = styled.div`
  display: flex;
`;

export const ButtonContainer = styled.div`
  display: grid;
`;
export const Description = styled.div`
  margin-left: 25px;
`;

export const Price = styled.div`
  font-size: 25px;
`;

export const Title = styled.div`
  font-size: 15px;
  font-weight: bold;
`;

export const Author = styled.div`
  font-size: 15px;
`;

export const Button = styled(AntdButton)`
  justify-self: center;
  width: 60%;
  height: 50px;
  margin-top: 50px;
  border-radius: 10px;
  background-color: #6699cc;
  border: none;
  :hover {
    background-color: #276c9b;
  }
`;

export const Text = styled.div`
  font-size: 20px;
  font-family: Courgette;
  color: rgb(255, 255, 255);
`;
