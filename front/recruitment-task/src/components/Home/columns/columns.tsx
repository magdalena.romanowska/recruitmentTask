import React from "react";
import { ColumnsType } from "antd/es/table";

import * as Styled from "../Home.styles";
import { Card } from "antd";

const { Meta } = Card;

interface IProps {
  addBook: (id: number) => void;
  renderTitle: (title: string) => JSX.Element[];
}

export const Columns = ({
  addBook,
  renderTitle,
}: IProps): ColumnsType<Book> => [
  {
    render: (_, { cover_url, title, author, pages, id }) => (
      <Styled.ContainerCard>
        <Styled.Card
          hoverable
          bordered={true}
          cover={<Styled.Image src={cover_url} />}
        >
          <Meta
            title={renderTitle(title)}
            description={
              <>
                <Styled.Description>{author}</Styled.Description>
                <Styled.Description>Liczba stron: {pages}</Styled.Description>
              </>
            }
          />
        </Styled.Card>
        <Styled.AddBookButton onClick={() => addBook(id)}>
          <Styled.Text>Dodaj do koszyka</Styled.Text>
        </Styled.AddBookButton>
      </Styled.ContainerCard>
    ),
  },
];
