import { createGlobalStyle } from "styled-components";

export const Roboto = createGlobalStyle`
  body {
    font-family: 'Roboto', sans-serif;
  }
`;
