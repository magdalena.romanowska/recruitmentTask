export const mockBooks: Book = {
  id: 457,
  title: "Matematyka 1. Podręcznik. Zakres podstawowy",
  author: "M. Karpiński, M. Dobrowolska, M. Braun, J. Lech",
  cover_url: "http://localhost:3001/static/cover/book/457.jpg",
  pages: 280,
  price: 3200,
  currency: "PLN",
};

export const mockPurchasedBooks: PurchasedBooks[] = [
  {
    book: {
      id: 457,
      title: "Matematyka 1. Podręcznik. Zakres podstawowy",
      author: "M. Karpiński, M. Dobrowolska, M. Braun, J. Lech",
      cover_url: "http://localhost:3001/static/cover/book/457.jpg",
      pages: 280,
      price: 3200,
      currency: "PLN",
    },
    quantity: 1,
  },
];

export const mockMetaData: MetaDataBook = {
  page: 1,
  records_per_page: 10,
  total_records: 17,
};
