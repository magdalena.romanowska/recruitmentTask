import React, { FC, ReactNode, useEffect, useState } from "react";
import { queryBooks } from "../services/book";

interface IProps {
  books: Book[];
  pageData?: MetaDataBook;
  purchasedBooks: PurchasedBooks[];
  addBookToBasket: (id: number) => void;
  addBook: (id: number) => void;
  deleteBook: (id: number) => void;
  addPricesOrder: () => ReactNode;
  cleanBasket: () => void;
}

const BookContext = React.createContext<IProps>({} as IProps);

const BookProvider: FC = (props) => {
  const [books, setBooks] = useState<Book[]>([]);
  const [pageData, setPageData] = useState<MetaDataBook>();
  const [purchasedBooks, setPurchasedBooks] = useState<PurchasedBooks[]>([]);

  useEffect(() => {
    queryBooks().then((response) => {
      if (response) {
        const { data, metadata } = response;
        setBooks(data);
        setPageData(metadata);
      }
    });
    setLocalStorageBooks();
  }, []);

  const setLocalStorageBooks = () => {
    const localStorageBooks = localStorage.getItem("purchasedBooks");
    const localStoragePurchasedBooks =
      localStorageBooks && JSON.parse(localStorageBooks);

    if (localStoragePurchasedBooks)
      setPurchasedBooks(localStoragePurchasedBooks);
  };

  const addBookToBasket = (idBook: number) => {
    const book = books.find(({ id }) => id === idBook);
    const orderedBook = purchasedBooks.find(({ book }) => book.id === idBook);

    if (orderedBook) {
      const otherBooks = purchasedBooks.filter(
        ({ book }) => book.id !== idBook
      );

      const payload: PurchasedBooks = {
        ...orderedBook,
        quantity: orderedBook.quantity + 1,
      };

      localStorage.setItem(
        "purchasedBooks",
        JSON.stringify([...otherBooks, payload])
      );

      setPurchasedBooks([...otherBooks, payload]);
    } else {
      if (book) {
        const payload: PurchasedBooks = {
          book: book,
          quantity: 1,
        };

        localStorage.setItem(
          "purchasedBooks",
          JSON.stringify([...purchasedBooks, payload])
        );

        setPurchasedBooks([...purchasedBooks, payload]);
      }
    }
  };

  const addBook = (idBook: number) => {
    const copyPurchasedBooks = [...purchasedBooks];

    const foundBook = copyPurchasedBooks.find(({ book }) => book.id === idBook);
    const indexFoundBook = copyPurchasedBooks.findIndex(
      ({ book }) => book.id === idBook
    );

    if (foundBook) {
      const payload: PurchasedBooks = {
        ...foundBook,
        quantity: foundBook.quantity + 1,
      };

      copyPurchasedBooks.splice(indexFoundBook, 1, payload);

      localStorage.setItem(
        "purchasedBooks",
        JSON.stringify(copyPurchasedBooks)
      );

      setPurchasedBooks(copyPurchasedBooks);
    }
  };

  const deleteBook = (idBook: number) => {
    const copyPurchasedBooks = [...purchasedBooks];

    const foundBook = copyPurchasedBooks.find(({ book }) => book.id === idBook);
    const otherBooks = copyPurchasedBooks.filter(
      ({ book }) => book.id !== idBook
    );
    const indexFoundBook = purchasedBooks.findIndex(
      ({ book }) => book.id === idBook
    );

    if (foundBook && indexFoundBook !== -1) {
      const payload: PurchasedBooks = {
        ...foundBook,
        quantity: foundBook.quantity > 0 ? foundBook.quantity - 1 : 0,
      };

      if (!payload.quantity) {
        localStorage.setItem("purchasedBooks", JSON.stringify(otherBooks));

        setPurchasedBooks(otherBooks);
      } else {
        copyPurchasedBooks.splice(indexFoundBook, 1, payload);

        localStorage.setItem(
          "purchasedBooks",
          JSON.stringify(copyPurchasedBooks)
        );

        setPurchasedBooks(copyPurchasedBooks);
      }
    }
  };

  const addPricesOrder = () => {
    const price =
      purchasedBooks.length &&
      purchasedBooks
        .map(({ book, quantity }) => book.price * quantity)
        .reduce((previousValue, currentValue) => previousValue + currentValue);

    return price && parseFloat((price / 100).toString()).toFixed(2);
  };

  const cleanBasket = () => {
    setPurchasedBooks([]);
    localStorage.clear();
  };

  const value = {
    books,
    pageData,
    purchasedBooks,
    addBookToBasket,
    addBook,
    deleteBook,
    addPricesOrder,
    cleanBasket,
  };

  return <BookContext.Provider value={value} {...props} />;
};

const useBook = () => {
  const context = React.useContext(BookContext);

  if (context === undefined) {
    throw new Error(`useBook must be used within a BookProvider`);
  }

  return context;
};

export { useBook, BookProvider, BookContext };
