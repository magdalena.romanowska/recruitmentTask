interface Book {
  author: string;
  cover_url: string;
  currency: string;
  id: number;
  pages: number;
  price: number;
  title: string;
}

interface MetaDataBook {
  page: number;
  records_per_page: number;
  total_records: number;
}

interface ResponseBook {
  data: Book[];
  metadata: MetaDataBook;
}

interface Order {
  id: number;
  quantity: number;
}

interface PayloadOrder {
  order: Order[];
  first_name: string;
  last_name: string;
  city: string;
  zip_code: number;
}

interface PurchasedBooks {
  book: Book;
  quantity: number;
}
