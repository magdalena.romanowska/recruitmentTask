export const queryBooks = (): Promise<ResponseBook> =>
  fetch("http://localhost:3001/api/book", {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  })
    .then((response) => response.json())
    .catch((error) => {
      throw error;
    });

export const submitOrder = (payload: PayloadOrder) =>
  fetch("http://localhost:3001/api/order", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  })
    .then((response) => response.json())
    .catch((error) => {
      console.error("Error:", error);
    });
