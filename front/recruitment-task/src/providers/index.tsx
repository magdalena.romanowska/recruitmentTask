import * as React from "react";
import { ReactElement } from "react";
import { BookProvider } from "../context/bookContext";

interface IProps {
  children: ReactElement;
}

export const AppProvider = ({ children }: IProps) => (
  <BookProvider>{children}</BookProvider>
);
